<?php
  require_once 'config.inc.php'; 	
  require_once 'twig/twig.init.php';
  require_once 'Main.class.php';

  $main = new Main();
  $twig = new Twig("templates", $main -> tpl, $main -> render);

  $twig -> load();	