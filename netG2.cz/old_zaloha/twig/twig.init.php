<?php
/**
 * @author Lubos Hubacek lubos.hubacek@diginex.cz
 * @date 21.6.2015
 */
class Twig{
	/** Adresar se sablonami */
	private $tpl_dir;
	/** Nazev sablony k vykresleni */
	private $tpl_load;
	/** Parametry pro predani sablone */
	private $tpl_render;
	
	public function __construct($tpl_dir, $tpl_load, $tpl_render){
		$this -> tpl_dir = $tpl_dir;
		$this -> tpl_load = $tpl_load;
		$this -> tpl_render = $tpl_render;
	}
	
	/**
	 * Pripravi sablonovaci system Twig; udajne bez cachovani
	 * Cesta k slozce s template je relativni od umisteni index.php
	 */ 		
	public function load(){
		require_once 'lib/Twig/Autoloader.php';
			 
		Twig_Autoloader::register();
		$loader = new Twig_Loader_Filesystem($this -> tpl_dir);
		$twig = new Twig_Environment($loader); 

    // added 28.7.2015
    try{
      $template = $twig->loadTemplate($this -> tpl_load);  
      echo $template->render($this -> tpl_render);	      
    }
    catch(Exception $e){ //Twig_Error_Loader
      header("HTTP/1.0 404 Not Found");
      //var_dump($e);
      echo "Error 404 - stránka nebyla nalezena";  
    }
	}
}