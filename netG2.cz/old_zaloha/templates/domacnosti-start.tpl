{% extends "include/index.tpl" %}

{% set cena_montaze = '2 099' %}
{% block content %}
	<section>
        <div id="prekryti-prava" onclick="location.href='/'"></div>
		<div id="leva-e">
            <div id="padesat">
			<main class="odraz">
				<header>
					<nav>
						<a href="/">ÚVOD</a><a href="procnetg2">PROČ netG2?</a><a href="kontakt">KONTAKT</a>
					</nav>
				</header>
				<div id="posun">
				<h1>Neomezený tarif pro nenáročné</h1>
				<div id="info">
					<div id="leva">
						Download<br />
						<b>2 Mbps</b>
					</div>
					<div id="prava">
						Upload<br />
						<b>1 Mbps</b>
					</div>
				</div>
			</div>
        </div>
			</main>
			<div class="nazev">
				<div id="sipka-leva">
					<a href="domacnosti-super" onMouseOver="document.zmen.src='templates/img/arrow_red_left.png';" onMouseOut="document.zmen.src='templates/img/arrow_red_left1.png';"><img src="templates/img/arrow_red_left1.png" name="zmen"></a> 
				</div>
				<div id="obal">
					<div id="tarif">
						Tarif
					</div>
					<div id="posun">
					<h2>Start</h2>
					</div>
					<div id="pocet">
						<a href="domacnosti-start"><img src="templates/img/tarif_strenght_red.png" alt=''><img src="templates/img/tarif_strenght_red.png" alt=''></a><a href="domacnosti-optimal"><img src="templates/img/tarif_strenght_white.png" alt=''><img src="templates/img/tarif_strenght_white.png" alt=''></a><a href="domacnosti-super"><img src="templates/img/tarif_strenght_white.png" alt=''><img src="templates/img/tarif_strenght_white.png" alt=''></a>
					</div>
				</div>
				<div id="sipka-prava">
					<a href="domacnosti-optimal" onMouseOver="document.zmen1.src='templates/img/arrow_red_right.png';" onMouseOut="document.zmen1.src='templates/img/arrow_red_right1.png';"><img src="templates/img/arrow_red_right1.png" name="zmen1" alt=''></a> 
				</div>
			</div>
			<main class="popis">
				<div class="vetsi" style="width:100%;height:60px;"></div>
				<div id="posun">
					{% embed "include/tarif-poznamka.tpl" %}{% endembed %}
          <div id="objednej" onclick="location.href='objednat-start'">
            <h5>Objednat za</h5>
            <h3>149,-</h3>
            <h4>Kč/měsíc</h4>
          </div>
				</div>
			</main>
      {% embed "include/footer.tpl" %}{% endembed %}
		</div>
		<div id="prava-f">
			<div class="nazev">
				<img src="templates/img/logo_white_pages.png" alt=""><h2>Domácnosti</h2>
			</div>
		</div>
	</section>
{% endblock %}