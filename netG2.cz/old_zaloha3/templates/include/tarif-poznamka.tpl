Cena za připojení s tímto tarifem je {{ cena_montaze }},- Kč.<br />
V ceně je zahrnuto kompletní vybavení pro příjem bezdrátového internetu,<br /> 
včetně kabeláže, dopravy a práce. Pokud již vlastníte kompatibilní zařízení<br />
bude vám automaticky poskytnuta sleva na zařízení Vaší přípojky.<br />