<?php
/**
 * Router aplikace
 */
class Main {
  /** Argument page z URL */
  private $get;

  /** Nazev sablony, ktera se bude renderovat */
  public $tpl = NULL;

  /** Pole argumentu, ktere se predaji sablonovacimu systemu */
  public $render = array();

  /**
   * Hlavni metoda aplikace
   */
  public function __construct() {
    $this -> get = @$_GET['page'];

    # vyber stranky dle GETu
    switch ($this->get) {
      case '':
      case 'uvod': $this -> page_index();
        break;
      case 'procnetg2': $this -> page_proc();
        break;
      case 'kontakt': $this -> page_kontakt();
        break;
      case 'domacnosti-start': $this -> page_start();
        break;
      case 'domacnosti-optimal': $this -> page_optimal();
        break;
      case 'domacnosti-super': $this -> page_super();
        break;
      case 'objednat-start': $this -> objednat('start');
        break;
      case 'objednat-optimal': $this -> objednat('optimal');
        break;
      case 'objednat-super': $this -> objednat('super');
        break;
      case 'mapa': $this -> page_mapa();
        break;
      case 'firmy': $this -> page_firmy();
        break;
      
      default: $this->errorPage(404);
    }
    
    # pripona
    $this -> tpl .= ".tpl";

    # parametry sablony
    $this->render['get'] = $this -> get;
  }
  
  # Uvodni
  private function page_index(){
    $this -> tpl = 'uvod';
    $this -> render['title'] = 'Připojení k internetu';
  }
  
  private function page_proc(){
    $this -> tpl = 'procnetg2';
    $this -> render['title'] = 'Proč my?';
  }
  
  private function page_kontakt(){
    $this -> tpl = 'kontakt';
    $this -> render['title'] = 'Kontakt';
  }
  
  # Tarify
  private function page_start(){
    $this -> tpl = 'domacnosti-start';
    $this -> render['title'] = 'Tarif Start';
  }
  
  private function page_optimal(){
    $this -> tpl = 'domacnosti-optimal';
    $this -> render['title'] = 'Tarif Optimal';
  }
  
  private function page_super(){
    $this -> tpl = 'domacnosti-super';
    $this -> render['title'] = 'Tarif Super';
  }
  
  # Objednavky
	private function objednat($typ){
		$this -> tpl = 'objednat';
    $tarif = ucFirst($typ);
    $this -> render['title'] = 'Objednat tarif ' . $tarif;
    $this -> render['tarif'] = $tarif;

		# objednavky
		if($_POST){
			foreach($_POST as $key => $value){
				$$key = htmlSpecialChars($value);
			}
			
			# PHP kontrola dat
      if($mesto == "" || $ulice == "" || $email == "" || $jmeno == ""){
        $this -> render["ok"] = "Nejsou vyplněny všechny povinné položky!";
        return;
      }
			
			$header = 'Content-type: text/html; charset=UTF-8' . "\r\n" . "From: WEB netG2 <" . $email . ">\r\n";
			$subject = "netG2 - objednávka";
			
			$text = ""
				. "Právě přišla nová nezávazná objednávka, přijali jsme následující údaje:<br /><br />"
				. "Jméno: $jmeno <br />"
				. "E-mail: $email <br />"
				. "Telefon: $telefon <br />"
				. "Město: $mesto <br />"
				. "Ulice: $ulice <br />"
				. "PSČ: $psc <br />"
				. "<strong>Tarif: $tarif </strong><br />"
				. "<br />Zpráva automaticky zaslána na adresy: " . MAIL_OSOBY;
				;
			
			$odeslano = mail(MAIL_OSOBY, $subject, $text, $header);
			if($odeslano)
				$this -> render["ok"] = "Úspešně odesláno! Náš obchodní zástupce vás brzy kontaktuje.";		
		}
	}
	
  # Dalsi
  private function page_mapa(){
    $this -> tpl = 'mapa-pokryti';
    $this -> render['title'] = 'Mapa pokrytí';
  }
  
  private function page_firmy(){
    $this -> tpl = 'firmy';
    $this -> render['title'] = 'Pro firmy';
		
		# objednavkovy form firmy
		if($_POST){
			foreach($_POST as $key => $value){
				$$key = htmlSpecialChars($value);
			}
      
			# PHP kontrola dat
      if($mesto == "" || $ulice == "" || $email == "" || $nazev == ""){
        $this -> render["ok"] = "Nejsou vyplněny všechny povinné položky!";
        return;
      }
			
			$header = 'Content-type: text/html; charset=UTF-8' . "\r\n" . "From: WEB netG2 <" . $email . ">\r\n";
			$subject = "netG2 - firemní objednávka";
			
			$text = ""
				. "Právě přišla nová nezávazná objednávka pro firmu, přijali jsme následující údaje:<br /><br />"
				. "Název firmy: $nazev <br />"
				. "E-mail: $email <br />"
				. "Telefon: $telefon <br />"
				. "Město: $mesto <br />"
				. "Ulice: $ulice <br />"
				. "PSČ: $psc <br />"
				. "<br />Zpráva automaticky zaslána na adresy: " . MAIL_FIRMY;
				;
			
			$odeslano = mail(MAIL_FIRMY, $subject, $text, $header);
			if($odeslano)
				$this -> render["ok"] = "Úspešně odesláno! Náš obchodní zástupce vás brzy kontaktuje.";
		}
  }


  /**
   *
   * @param int $error
   */
  private function errorPage($error) {
    
  }

}
