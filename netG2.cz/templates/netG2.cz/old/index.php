<?php
error_reporting(E_ALL);
if (isset($_GET["page"])) {
    $page = strtolower($_GET["page"]);
} else {
    $page = "main";
}

?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="/images/icon.png" rel="icon" type="image/png" />
        <meta name="dc.language" content="cs" />
        <meta name="geo.country" content="CZ" />
        <title>Internet netG2 - Rokycany</title>
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <link rel="stylesheet" type="text/css" href="style-css/<? echo $page; ?>.css" />
        <meta name="author" content="Diginex - Petr Pešek" />
        <meta name='robots' content='all, index, follow, archive' />  
        <meta name="googlebot" content="index,follow,snippet" />
        <meta name="description" lang="cs" content="Rychlý a stabilní internet pro domácnosti i firmy - netG2, Rokycany" />
        <meta name="keywords" content="internet, levný, rychlý, Rokycany, Kamenný Újezd, Hrádek, Dobřív, Mirošov, netG2, Diginex" />  
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-36094366-3', 'netg2.cz');
            ga('send', 'pageview');

        </script>
    </head>
    <body>
        <div id="page">
            <div id="head">
                <a href='index.php' id="logo"><img src="images/logo_netg2.png" alt="Rychlý internet netG2"/></a>
                <div id="napis_hlavni"><label><strong>Volejte: 608 308 600</strong></label></div>
            </div>
            <div id="body">
                <?php
                if (isset($_GET["page"])) {
                    if (file_exists("pages/" . $page . ".php") == true) {
                        include "pages/" . $page . ".php";
                    } else {
                        include "pages/main.php";
                    }
                } else {
                    include "pages/main.php";
                }
                ?>
                <div id="zapati"> 
                  <a class="odkaz_podminky" href="vseobecne_podminky.pdf">Všeobecné podmínky (62kB)</a>, 
                  <a class="odkaz_podminky" href="rozhrani.pdf">Rozhraní (143kB)</a>
                </div>
            </div>
            <div id="pata">
                <div id="diginex_logo"></div>
                <div id="text"><label>Diginex Technology s.r.o. | <a href="http://www.diginex.cz" title="Diginex Technology s.r.o.">www.diginex.cz</a></label></div>
            </div>
        </div>

    </body>
</html>