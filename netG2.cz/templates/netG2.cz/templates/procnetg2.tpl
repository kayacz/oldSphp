{% extends "include/index.tpl" %}

{% block content %}
    <div id="prekryti-leva" onclick="location.href='/'"></div>
	<div id="section">
		<div id="leva-f">
			<div class="nazev">
				<img src="templates/img/logo_white_pages.png" alt=""><h2>Proč netG2?</h2>
			</div>
		</div>
		<div id="prava-e">
      <div id="padesat" style='height: auto !important;'>
        <main style='padding-bottom: 0px !important;'>
          <header>
            <nav>
              <a href="/">ÚVOD</a><a href="procnetg2">PROČ netG2?</a><a href="kontakt">KONTAKT</a>
            </nav>
          </header>
        </main>
      </div>

            <div id="proc">
                <h1>Proč netG2?</h1>
                <span class="bold">
                Nejnovější infrastruktura s vysokým výkonem a stabilitou na Rokycansku<br />
                Při rozsáhlém výpadku proudu internet nadále funguje minimálně 10 hodin<br />
                Vysoká stabilita i v době večerní špičky samozřejmostí<br />
                Zázemí dynamicky se rozvíjející technologické firmy<br />
                Věrnostní program DIGINEX Benefits<br /><br />
                </span>
                <div class="blok">
                Internet netG2 se vyznačuje vysokou stabilitou a to i v době večerních špiček, kdy je k síti připojeno nejvíce lidí. Snažíme se naši infrastrukturu maximálně předimenzovat, aby i vyšší nápor požadavků byl bez větších problémů obsloužen. Tohoto dosahujeme striktně nastavenými tarify, které každému uživateli dávají jen takovou porci kapacity, kterou si zaplatil. Proto Vám můžeme zaručit, že naše připojení bude velmi stabilní v jakoukoli denní dobu, za jakéhokoliv počasí. A to je fér!
                </div>
                <div class="blok">
                Zvolíte-li naše připojení, stáváte se zákazníkem firmy, která nabízí komplexní IT služby. Co to znamená? Můžete čerpat různé výhody za doporučení naší firmy Vaším přátelům a známým, stanete se členem našeho věrnostního programu a v neposlední řadě zíkáte slevy na další produkty a služby firmy DIGINEX Technology s.r.o. Dokonce si můžete u nás i něco vydělat.
                </div>
                <div class="blok">
                <strong>Aktuálně internet netG2 naleznete ve městech Rokycany, Mirošov a Hrádek a v okolních obcích Kamenný Újezd, Hrádek, Volduchy, Dobřív, Pavlovsko, Svojkovice, Vitinka, Skořice a Příkosice.</strong> Naše pokrytí si můžete rovněž prohlídnout na <a href="mapa-pokryti.jpg">mapě pokrytí</a>. Naši působnost díky Vašemu velkému zájmu neustále rozšiřujeme dál a dál. Pokud máte zájem se na rozšíření působnosti této internetové sítě podílet, zašlete nám e-mail nebo nás kontaktujte telefonicky. Získáte tímto počinem připojení k internetu po celou dobu spolupráce <strong>ZDARMA!</strong>
                </div>
            </div>
			{% embed "include/footer.tpl" %}{% endembed %}
		</div>
	</div>
{% endblock %}
{% block description %}
Proč zvolit k připojení k internetu právě naší firmu? Máme nejnovější infrastrukturu, kvalitní servis, vysokou stabilitu i v době večerních špiček. Jsme dynamicky se rozvíjející místní firma mladých a perspektivních lidí. Pojďte to s námi zkusit.
{% endblock %}