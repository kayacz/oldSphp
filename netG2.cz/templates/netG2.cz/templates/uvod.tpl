{% extends "include/index.tpl" %}

{% block content %}
	<div id="main">
		<div id="leva">
			<div class="vyber">
				<a class="leva" href="firmy">Firmy / Organizace</a>
			</div>
		</div>
		<div id="prava">
			<div class="vyber">
				<a class="prava" href="domacnosti-start">Domácnosti</a>
			</div>
		</div>
	</div>
	<div id="header">
		<div id='testik' style="
    margin: 0 auto;
    width: auto;
">
		<div id="left" style='float: left !important; display: inline-block !important;'>
			 <a href='http://netg2.cz'><img class="logouvod" src="templates/img/logo_white_home.png" alt="netG2 logo"></a>
		</div>
		<div id="right" style='float: left !important; display: inline-block !important;'>
			Rychlý internet v Rokycanech<br /> a okolí,
			sítě na míru, telefonie,<br /> kamerové systémy a další ...
		</div>
		</div>
		<div id="odkazy">
			<a href="procnetg2">PROČ netG2?</a>
      
			<a href="kontakt" style='text-transform: uppercase;'>Kontakt</a>
		</div>
	</div>
{% endblock %}
{% block description %}
Nabízíme stabilní a rychlý internet pro domácnosti i firmy na Rokycansku. Hlavní důraz klademe na kvalitní a rychlý servis a bezproblémový a svižný chod našeho připojení. Neváhejte a vyzkoušejte!
{% endblock %}