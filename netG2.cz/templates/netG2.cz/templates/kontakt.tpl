{% extends "include/index.tpl" %}

{% block head %}
  <script src="http://maps.googleapis.com/maps/api/js"></script>
  <script>
  var myCenter=new google.maps.LatLng(49.722458,13.632221);

  function initialize()
  {
  var mapProp = {
    center: {lat: 49.723150, lng: 13.632221},
    zoom:17,
    disableDefaultUI:true,
    mapTypeId:google.maps.MapTypeId.ROADMAP,
    styles: [{"featureType":"landscape","stylers":[{"saturation":-100},{"lightness":0},{"visibility":"on"}]},{"featureType":"poi","stylers":[{"saturation":-100},{"lightness":51},{"visibility":"simplified"}]},{"featureType":"road.highway","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"road.arterial","stylers":[{"saturation":-100},{"lightness":30},{"visibility":"on"}]},{"featureType":"road.local","stylers":[{"saturation":-100},{"lightness":40},{"visibility":"on"}]},{"featureType":"transit","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"administrative.province","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":-25},{"saturation":-100}]},{"featureType":"water","elementType":"geometry","stylers":[{"hue":"#ffff00"},{"lightness":-25},{"saturation":-97}]}]
    };

  var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);

  var marker=new google.maps.Marker({
    position:myCenter,
    icon:'templates/img/ukaz1.png'
    });

  marker.setMap(map);
  }

  google.maps.event.addDomListener(window, 'load', initialize);


  </script>
{% endblock %}

{% block content %}
<div id="section">
	<div class="kontakt">
  
			<div id="googleMap"></div>

    <div id="vycentruj">
			<div id="kontakt">
				<h1>Kontakt</h1>
			</div>
    </div>
				
				    
    <main class="dalsi" style='padding-bottom: 0px !important;'>
      <header style='padding-top: 0px !important;'>
        <nav>
          <a href="/">ÚVOD</a>
					<a href="procnetg2">PROČ netG2?</a>
					<a href="mapa-pokryti.jpg" target='_blank'>Mapa pokrytí</a>
        </nav>
      </header>

			DIGINEX Technology s.r.o.<br />
			Kocanda 20, 338 42 Hrádek<br />
			IČ: 040 12 291<br /><br />
			Petr Pešek:<br />
			<strong>+420 608 308 600</strong><br />
			<strong>petr.pesek@diginex.cz</strong>
		</main>
		{% embed "include/footer.tpl" %}{% endembed %}
	</div>
    </div>
{% endblock %}
{% block description %}
Internet netG2 - Rokycany, Kamenný Újezd, Mirošov, Hrádek, Volduchy, Osek, Svojkovice, Skořice, Příkosice, Dobřív, Pavlovsko. Nevájte nás kontaktovat! Neváhejte se k nám připojit!
{% endblock %}