<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>{{ title }} | netG2</title>
	<link rel="stylesheet" href="templates/css/style.css">
	<link rel="icon" href="templates/img/favicon.png">
  <meta name='author' content='DIGINEX Technology s.r.o.' /> 
  <meta name='robots' content='all, index, follow, archive' />        
  <meta name="description" lang="cs" content="Rychlý a stabilní internet pro domácnosti i firmy - netG2, Rokycany" />
  <meta name="keywords" content="internet, levný, rychlý, Rokycany, Kamenný Újezd, Hrádek, Dobřív, Mirošov, netG2" />    
  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    
    ga('create', 'UA-36094366-3', 'auto');
    ga('send', 'pageview');
    
  </script>
  {% block head %}{% endblock %}
</head>
<body>
  <noscript>Pro plnohodnotné používání webu musíte mít zapnutou technologii JavaScript!</noscript>
  {% block content %}{% endblock %}
</body>
</html>