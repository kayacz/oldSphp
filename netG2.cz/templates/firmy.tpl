{% extends "include/index.tpl" %}

{% block content %}
    <div id="prekryti-leva" onclick="location.href='/'"></div>
	<div id="section">
		<div id="leva-f">
			<div class="nazev">
				<img src="templates/img/logo_white_pages.png" alt=""><h2>Organizace / firmy</h2>
			</div>
		</div>
		<div id="prava-e">
            <div id="padesat">
			<main>
				<header>
					<nav>
						<a href="/">ÚVOD</a><a href="procnetg2">PROČ netG2?</a><a href="kontakt">KONTAKT</a>
					</nav>
				</header>
				<h1>Spojíme se s vámi a navrhneme nejlepší řešení</h1>
				<div id="popis">
					Nabízíme služby v oblasti sítí na míru, poskytujeme internet,<br /> navrhujeme a dodáváme strukturované sítě, Wi-Fi,<br />kamerové systémy a mnoho dalšího.
				</div>
			</main>
            </div>
			<div class="nazev">
				<h2>Zašlete nám kontakt na vás</h2>
			</div>
			<div id="main">
				<form method='post'>
					<div id="objednavka">
						<div id="levaa">
							<input type="text" name="nazev" placeholder="název firmy*" required><br />
							<input type="text" name="telefon" placeholder="telefon"><br />
							<input type="text" name="email" placeholder="e-mail*" required>
						</div>
						<div id="pravaa">
							<input type="text" name="ulice" placeholder="ulice a č.p.*" required><br />
							<input type="text" name="mesto" placeholder="město*" required><br />
							<input type="text" name="psc" placeholder="PSČ">
						</div>
					</div>
          
          {% if ok != "" %}
            <div class='dialog'>{{ ok }}</div>
          {% endif %}
          
					<input type="submit" value="Odeslat">
				</form>
			</div>
			{% embed "include/footer.tpl" %}{% endembed %}
		</div>
	</div>
{% endblock %}
{% block description %}
Pro firemní zákazníky nabízíme individuální řešení, individuální tarify, kamerové systémy, instalace sítí, správu sítí a mnoho dalšího. Neváhejte nás kontaktovat zkrze náš on-line formulář.
{% endblock %}