<?php
  session_start();

  if(!headers_sent()) {		 
    header('Content-Type: text/html; charset=utf-8');		
  }
  
	# chybova hlaseni
	ini_set("display_errors", 1);
	ini_set("track_errors", 1);
	ini_set("html_errors", 1);
	error_reporting(E_ALL);
	
	# casove pasmo
	date_default_timezone_set('Europe/Prague');
  
  # ostatni konstanty 
  define("MAIL_FIRMY", "petr.pesek@diginex.cz,lubos.hubacek@diginex.cz"); // vice mailu oddelenych carkou
	define("MAIL_OSOBY", "petr.pesek@diginex.cz"); // vice mailu oddelenych carkou
