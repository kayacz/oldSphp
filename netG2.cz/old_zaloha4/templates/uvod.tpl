{% extends "include/index.tpl" %}

{% block content %}
	<div id="main">
		<div id="leva">
			<div class="vyber">
				<a class="leva" href="firmy">Firmy / Organizace</a>
			</div>
		</div>
		<div id="prava">
			<div class="vyber">
				<a class="prava" href="domacnosti-start">Domácnosti</a>
			</div>
		</div>
	</div>
	<div id="header">
		<div id="left">
			<a href='http://netg2.cz'><img src="templates/img/logo_white_home.png" height="100" alt="netG2 logo"></a>
		</div>
		<div id="right">
			rychlý internet, sítě na míru,<br />
			telefonie, kamerové systémy
		</div>
		<div id="odkazy">
			<a href="procnetg2">PROČ netG2?</a>
			<a href="kontakt" style='text-transform: uppercase;'>Kontakt</a>
		</div>
	</div>
{% endblock %}