{% extends "include/index.tpl" %}

{% block content %}
    <div id="prekryti-prava" onclick="location.href='/'"></div>
	<div id="section">
		<div id="leva-e">
      <div id="padesat">
        <main>
          <header>
            <nav>
              <a href="/">ÚVOD</a><a href="procnetg2">PROČ netG2?</a><a href="kontakt">KONTAKT</a>
            </nav>
          </header>
          <h1>Nezávazná objednávka</h1>
          <div id="popis">
            Nejedná se o závaznou objednávku, formulář slouží jako poptávka,<br /> na základě které Vás budeme kontaktovat a sdělíme Vám další informace.
          </div>
        </main>
      </div>
			<div class="nazev">
				<div id="obal" style="width:50%;">
					<div id="tarif">
						Tarif
					</div>
					<h2>{{ tarif }}</h2>
					<div id="pocet">
            <a href="objednat-start"><img src="templates/img/tarif_strenght_red.png" alt=''><img src="templates/img/tarif_strenght_red.png" alt=''></a>{% if tarif == 'Super' or tarif == 'Optimal' %}<a href="objednat-optimal"><img src="templates/img/tarif_strenght_red.png" alt=''><img src="templates/img/tarif_strenght_red.png" alt=''></a>{% else %}<a href="objednat-optimal"><img src="templates/img/tarif_strenght_white.png" alt=''><img src="templates/img/tarif_strenght_white.png" alt=''></a>{% endif %}{% if tarif == 'Super' %}<a href="objednat-super"><img src="templates/img/tarif_strenght_red.png" alt=''><img src="templates/img/tarif_strenght_red.png" alt=''></a>{% else %}<a href="objednat-super"><img src="templates/img/tarif_strenght_white.png" alt=''><img src="templates/img/tarif_strenght_white.png" alt=''></a>{% endif %}
          </div>
				</div>
			</div>
			<div id="main">
				<form method='post'>
					<div id="objednavka">
						<div id="levaa">
							<input type="text" name="jmeno" placeholder="jméno a příjmení*" required><br />
							<input type="text" name="telefon" placeholder="telefon"><br />
							<input type="text" name="email" placeholder="e-mail*" required>
						</div>
						<div id="pravaa">
							<input type="text" name="ulice" placeholder="ulice a č.p.*" required><br />
							<input type="text" name="mesto" placeholder="město*" required><br />
							<input type="text" name="psc" placeholder="psč">
              <input type='hidden' name='tarif' value='{{ tarif }}'>
						</div>
					</div>
          
          {% if ok != "" %}
            <div class='dialog'>{{ ok }}</div>
          {% endif %}
          
					<input type="submit">
				</form>
			</div>
			{% embed "include/footer.tpl" %}{% endembed %}
		</div>
		<div id="prava-f">
			<div class="nazev">
				<img src="templates/img/logo_white_pages.png" alt="Logo netG2" /><h2>Domácnosti</h2>
			</div>
		</div>
	</div>
{% endblock %}