﻿<?php
if(isset($_GET['odeslano']) && $_GET['odeslano'] == "true")
    $odeslano = true;
else
    $odeslano = false;
?>
<div class="nadpis_body">
    <h1>Rychlé a spolehlivé připojení k internetu na Rokycansku</h1>
</div>
<div id="schema">
    <img src="images/schema_site.jpg" alt="Ukázka sítě netG2" />
    <div id="text_blok">
      <h2>netG2 je spolehlivý a rychlý internet.</h2>
      <p>
        Naší prioritou je především spolehlivost a dostupnost služby. U nás se vám nikdy nestane, 
        aby Vám internet vypadával, nebo byl pomalý jako šnek. Neslibujeme nesmysleně velké rychlosti, ale 
        <strong>stabilní připojení k internetu</strong>, které vám dá jistotu, že pokaždé, když zapnete počítač 
        připojení poběží a bude svižné.
        <!--
        Klademe důraz především na spolehlivost a dostupnost služby. 
        U nás se Vám nikdy nestane aby
        Vám internet vypadával či byl pomalý jako šnek. Neslibujeme Vám 
        nesmyslné rychlosti, nýbrž stabilitu a jistotu, že pokaždé, když zapnete
        svůj počítač, internet poběží a bude svižně reagovat na Vaše požadavky.
        -->
      </p>
    </div>
</div>
<div class="nadpis_body">
    
</div>
<div id="tarify">
    <div class="tarif">
        <div class="L1">
            <div class="L11">
                <div class="L111">
                    <label>tarif</label>
                </div>
                <div class="L112">
                    <label>START</label>
                </div>
            </div>
            <div class="L12"></div>
            <div class="L13">
                <label>2 Mbps</label>
            </div>
        </div>
        <div class="R1">
            <img src="images/budik_start.png" alt="Internet pro nenáročné uživatele"/> 
        </div>
        <div class="C1"></div>
        <div class="C2">
            <p>Vhodný pro připojení jednoho počítače</p>
        </div>
        <div class="C3">
            <div class="C31"><label>149,- Kč</label></div>
            <a href="index.php?page=order&amp;tarif=1"><div class="C32">VÍCE</div></a>
        </div>
    </div>
    <div class="tarif">
        <div class="L1">
            <div class="L11">
                <div class="L111">
                    <label>tarif</label>
                </div>
                <div class="L112">
                    <label>OPTIMAL</label>
                </div>
            </div>
            <div class="L12"></div>
            <div class="L13">
                <label>8 Mbps</label>
            </div>
        </div>
        <div class="R1">
            <img src="images/budik_optimal.png" alt="Internet pro domácnost"/> 
        </div>
        <div class="C1"></div>
        <div class="C2">
            <p>Vhodný pro rodinu s více zařízeními</p>
            <p class="doporucujeme">DOPORUČUJEME</p>
        </div>
        <div class="C3">
            <div class="C31"><label>299,- Kč</label></div>
            <a href="index.php?page=order&amp;tarif=2"><div class="C32">VÍCE</div></a>
        </div>
    </div>
    <div class="tarif">
        <div class="L1">
            <div class="L11">
                <div class="L111">
                    <label>tarif</label>
                </div>
                <div class="L112">
                    <label>SUPER</label>
                </div>
            </div>
            <div class="L12"></div>
            <div class="L13">
                <label>16 Mbps</label>
            </div>
        </div>
        <div class="R1">
            <img src="images/budik_super.png" alt="Internet pro náročné uživatele"/> 
        </div>
        <div class="C1"></div>
        <div class="C2">
            <p>Vhodný pro firmy a profesionály nebo náročné uživatele</p>
        </div>
        <div class="C3">
            <div class="C31"><label>549,- Kč</label></div>
            <a href="index.php?page=order&amp;tarif=3"><div class="C32">VÍCE</div></a>
        </div>
    </div>
</div>
<div class="nadpis_body">
    <h2>Pokrytí bezdrátovou sítí</h2>
</div>
<div id="text_pokryti">
    <p>
      Připojení k naší rychlé wi-fi síti je dostupné v mnoha obcích na Rokycansku. Zejména jde o <strong>Rokycany</strong>, <strong>Kamenný Újezd</strong>, 
      <strong>Hrádek u Rokycan</strong>, <strong>Dobřív</strong> a <strong>Mirošov</strong>. <strong>Nově působíme ve Volduchách, Svojkovicích a na Oseku!</strong> Naší internetovou síť však i 
      díky kladným ohlasům našich zákazníků neustále rozšiřujeme do nových lokalit. Proto nám neváhejte poslat dotaz 
      s vaší adresou a my ověříme, zda-li by bylo možné k Vám naše vysílání rozšířit. V případě zájmu můžete využít i 
      naší infolinku na čísle 608 308 600. Těšíme se na vaše zavolání!
        <!--
        Rychlý internet netG2 je dostupný v Rokycanech, Kamenném Újezdu u Rokycan a Hrádku u Rokycan. 
        Na následující mapě je barevně vyznačeno naše pokrytí. Pokud se nenacházíte v pokrytém území, 
        zašlete nám dotaz s adresou a my ověříme, zda-li bychom k Vám naše vysílání rozšířili, popř. nás kontaktujte 
        na naší infolince.
        -->
    </p><br />
    <p style="text-align: center;">
      <strong>POZOR AKCE!! <br> Buďte mezi prvními zákazníky v nově pokryté lokalitě Volduchy, Osek a Svojkovice a získejte přístup k internetu ZDARMA. <u>Napořád!</u> Pro více informací volejte nebo pište prostřednictvím kontaktího formuláře níže.</strong>
    </p>
	<!--<p>Nově rychlým internetem pokrýváme Pavlovsko, Dobřív a Mirošov. V případě zájmu o internet v těchto lokalitách nám prosím napište vzkaz nebo zavolejte!</p>-->
</div>
<div id="mapa_pokryti"><a href="images/pokryti_velke.jpg" target="_blank"><img src="images/pokryti.jpg" alt="Mapa pokrytí netG2"/></a></div>
<form action="odeslat_netg2.php?dotaz=dotaz" method="post">
    <div id="body_part_4">
      <?php
        if (!$odeslano) {
            ?>
            <div class="nadpis"><h2>Napište nám</h2></div>
            <div class="form">

                <div class="radek">
                    <div class="polozka">Jméno a příjmení*</div>
                    <input class="policko" type="text" name="jmeno" required />
                </div>
                <div class="radek">
                    <div class="polozka">E-mail*</div>
                    <input class="policko" type="text" name="email" required />
                </div>
                <div class="radek">
                    <div class="polozka">Telefon</div>
                    <input class="policko" type="text" name="telefon" />
                </div>
                <div class="radek">
                    <div class="polozka">Váš vzkaz*</div>
                    <textarea name="vzkaz" class="policko_2" required></textarea>
                </div>
                <!--<div class="popisek">Položky označené * jsou povinné.</div>-->
                <div id="odeslat22">
                  <input type="submit" value="Odeslat vzkaz" />
                </div>
            </div>
    </div>


        <?php
    } else {
        ?>
        <div id='ok' class="nadpis"><h2>Váš vzkaz byl úspěšně odeslán!</h2></div>
      </div>
    <?php } ?>

</form/>