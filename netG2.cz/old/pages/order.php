<?php
if (isset($_GET['odeslano']) && $_GET['odeslano'] == "true")
    $odeslano = true;
else
    $odeslano = false;
    
    
  if ($_GET['tarif'] == "1") {
    ?>
    <div class="left">
        <div id="tarif">
            <div id="L1">
                <div id="L11">
                    <div id="L111">
                        <label>tarif</label>
                    </div>
                    <div id="L112">
                        <label>START</label>
                    </div>
                </div>
                <div id="L12"></div>
                <div id="L13">
                    <label class="down">2 Mbps</label><br />
                    <label class="up">512 kbps - odesílání</label>
                </div>
            </div>
            <div id="R1">
                <img src="images/budik_start.png" alt="Internet pro nenáročné uživatele"/> 
            </div>
            <div id="C1"></div>
            <div id="C2">
                <p>Tarif vhodný pro připojení jednoho počítače</p>
            </div>
            <div id="C3">
                <div id="C31"><label>149,- Kč/měsíc</label></div>
                <div id="C32">1 788,- Kč/rok</div>
            </div>
        </div>
        <div class="poznamka">
            <p>
                Cena za zřízení přípojky s tímto tarifem je 2 099,- Kč. Cena zahrnuje kompletní vybavení pro příjem bezdrátového internetu,
                kabeláž, dopravu a práci. Může být poskytnuta sleva, pokud již vlastníte podobné zařízení zakoupené od jiného operátora.
            </p>
        </div>
    </div>
<?php
  } 
  elseif ($_GET['tarif'] == "2"){
    ?>
<div class="left">
    <div id="tarif">
        <div id="L1">
            <div id="L11">
                <div id="L111">
                    <label>tarif</label>
                </div>
                <div id="L112">
                    <label>OPTIMAL</label>
                </div>
            </div>
            <div id="L12"></div>
            <div id="L13">
                <label class="down">8 Mbps</label><br />
                <label class="up">2 000 kbps - odesílání</label>
            </div>
        </div>
        <div id="R1">
            <img src="images/budik_optimal.png" alt="Internet pro domácnost"/> 
        </div>
        <div id="C1"></div>
        <div id="C2">
            <p>Tarif vhodný pro rodinu s více zařízeními</p>
        </div>
        <div id="C3">
            <div id="C31"><label>299,- Kč/měsíc</label></div>
            <div id="C32">3 588,- Kč/rok</div>
        </div>
    </div>
        <div class="poznamka">
            <p>
                Cena za zřízení přípojky s tímto tarifem je 1 849,- Kč. Cena zahrnuje kompletní vybavení pro příjem bezdrátového internetu,
                kabeláž, dopravu a práci. Může být poskytnuta sleva, pokud již vlastníte podobné zařízení zakoupené od jiného operátora.
            </p>
        </div>
</div>
    <?php
  }
  elseif ($_GET['tarif'] == "3"){
    ?>
<div class="left">
    <div id="tarif">
        <div id="L1">
            <div id="L11">
                <div id="L111">
                    <label>tarif</label>
                </div>
                <div id="L112">
                    <label>SUPER</label>
                </div>
            </div>
            <div id="L12"></div>
            <div id="L13">
                <label class="down">16 Mbps</label><br />
                <label class="up">4 000 kbps - odesílání</label>
            </div>
        </div>
        <div id="R1">
            <img src="images/budik_super.png" alt="Internet pro náročné uživatele"/> 
        </div>
        <div id="C1"></div>
        <div id="C2">
            <p>Tarif vhodný pro firmy a profesionály nebo náročné uživatele</p>
        </div>
        <div id="C3">
            <div id="C31"><label>549,- Kč/měsíc</label></div>
            <div id="C32">6 588,- Kč/rok</div>
        </div>
    </div>
        <div class="poznamka">
            <p>
                Cena za zřízení přípojky s tímto tarifem je 1 499,- Kč. Cena zahrnuje kompletní vybavení pro příjem bezdrátového internetu,
                kabeláž, dopravu a práci. Může být poskytnuta sleva, pokud již vlastníte podobné zařízení zakoupené od jiného operátora.
            </p>
        </div>
</div>    
    <?php
  }

?>

    <!-- spolecne -->
    <form action="http://www.netG2.cz/odeslat_netg2.php?&amp;tarif=<?=$_GET['tarif']?>&amp;objednavka=objednavka" method="post">
        <div id="body_part_4">
            <?php
            if (!$odeslano) {
                ?>
                <div class="nadpis"><h1>Objednávka</h1></div>
                <div class="form">

                    <div class="radek">
                        <div class="polozka">Jméno a příjmení*</div>
                        <input class="policko" type="text" name="jmeno" required />
                    </div>                  
                    <div class="radek">
                        <div class="polozka">E-mail*</div>
                        <input class="policko" type="text" name="email" required />
                    </div>
                    <div class="radek">
                        <div class="polozka">Telefon*</div>
                        <input class="policko" type="text" name="telefon" />
                    </div>
                    <div class="radek">
                        <div class="polozka">Ulice a č. p.*</div>
                        <input class="policko" type="text" name="ulice" required />
                    </div>
                    <div class="radek">
                        <div class="polozka">Město*</div>
                        <input class="policko" type="text" name="mesto" required />
                    </div>
                    <div class="radek">
                        <div class="polozka">PSČ*</div>
                        <input class="policko" type="text" name="psc" required />
                    </div>
                    <div class="radek">
                        <div class="polozka">Poznámka</div>
                        <textarea name="poznamka" class="policko_2"></textarea>
                    </div>
                    <!--<div class="popisek">Položky označené * jsou povinné.</div>-->
                    <div class="popisek"><strong>Nejedná se o závaznou objednávku</strong>, formulář slouží jako poptávka, na základě které Vás budeme kontaktovat a sdělíme Vám další informace.</div>
                </div>
                <div id="odeslat">
                    <input type="submit" value="Odeslat" />
                </div>

                <?php
            } else {
                ?>
                <div class="nadpis"><h1>Objednávka byla úspěšně odeslána!</h1></div>
            <?php } ?>
        </div>
    </form>
