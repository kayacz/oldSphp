<html>
<head>
<link rel="stylesheet" type="text/css" href="comment_style.css">
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script type="text/javascript">
function post()
{
  var comment = document.getElementById("comment").value;
  var name = document.getElementById("username").value;
  if(comment && name)
  {
    $.ajax
    ({
      type: 'post',
      url: 'post_comment.php',
      data: 
      {
         user_comm:comment,
	     user_name:name
      },
      success: function (response) 
      {
	    document.getElementById("all_comments").innerHTML=response+document.getElementById("all_comments").innerHTML;
	    document.getElementById("comment").value="";
        document.getElementById("username").value="";
  
      }
    });
  }
  
  return false;
}
</script>

</head>

<body>

  <h1>Instant Comment System Using Ajax,PHP and MySQL</h1>

  <form method='post' action="" onsubmit="return post();">
  <textarea id="comment" placeholder="Write Your Comment Here....."></textarea>
  <br>
  <input type="text" id="username" placeholder="Your Name">
  <br>
  <input type="submit" value="Post Comment">
  </form>

  <div id="all_comments">
  <?php
    $host="localhost";
    $username="root";
    $password="";
    $databasename="social_network";

    $connect=mysqli_connect($host,$username,$password,$databasename);
    
  
    $comm = "select name,comment,post_time from comments order by post_time desc";
    $result = mysqli_query($connect, $comm);

if (mysqli_num_rows($result) > 0) {
    while($row=mysqli_fetch_assoc($result))
    {
	  $name=$row['name'];
	  $comment=$row['comment'];
      $time=$row['post_time'];
    echo $name.$comment.$time;
 }  }
    ?>
  </div>

</body>
</html>