<?php
class Main {
	/** Argument page z URL */
	private $page;
	/** Argument action z URL */
	private $action;
  private $id;
	/** Nazev sablony, ktera se bude renderovat */
	public $tpl = NULL;
	/** Pole argumentu, ktere se predaji sablonovacimu systemu */
	public $render = array();
	
	/**
	 * Hlavni ridici metoda cele aplikace
	 */
	public function __construct() {
		$this->page = @$_GET['page'];
			switch($this->page){
				case 'o_nas': $this->o_nas();
					break;
				case 'tym': $this->tym();
					break;
				case 'firma': $this->firma();
					break;
        case 'kontakt': $this->kontakt();
					break;
        case 'servis': $this->servis_vyvoj();
					break;
        case 'vyvoj': $this->servis_vyvoj();
					break;
        case 'internet': $this->servis_vyvoj();
					break;
				default: $this->domovskaStranka();
        }
    $this->addExtension();
	}
  private function addExtension(){
		$this->tpl .= ".tpl";
	}
  private function servis_vyvoj(){
		$this->tpl .= $this->page;
	}
  private function domovskaStranka(){
		$this->tpl = 'home';
	}
  private function kontakt(){
		$this->tpl = 'kontakt';
	}
  private function o_nas(){
		$this->tpl = 'o_nas';
	}
	private function tym(){
		$this->tpl = 'tym';
	}
  private function firma(){
		$this->tpl = 'firma';
	}
}