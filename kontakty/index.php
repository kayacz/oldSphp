<?php 
  ini_set('display_errors', '1');
  require_once 'php/Main.class.php';
  require_once 'twig/lib/Twig/Autoloader.php';

	/*
	 * Pripravi sablonovaci system Twig; udajne bez cachovani
	 * Cesta k slozce s template je relativni od umisteni index.php
	 */
  Twig_Autoloader::register();
	$loader = new Twig_Loader_Filesystem('templates');
	$twig = new Twig_Environment($loader);
	 
	// zaregistruje funkci pro automaticke nacitani
	//spl_autoload_register("autoload");

  $main = new Main(); 
  $template = $twig->loadTemplate($main->tpl);
  echo $template->render($main->render);