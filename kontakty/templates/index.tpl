<!DOCTYPE HTML>
<html lang='cs'>
	<head>
		<title>DIGINEX - {% block title %}{% endblock %}</title>
		<meta charset="UTF-8">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href="graphics/css/index.css" />
		<link rel="shortcut icon" type="image/png" href="graphics/images/favicon.png"/>

		<style>


		</style>
    	<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-36094366-3', 'auto');
    ga('send', 'pageview');

  </script>
    <script src="http://maps.googleapis.com/maps/api/js"></script>
  <script>
  var myCenter=new google.maps.LatLng(49.7222767,13.6322122);

  function initialize()
  {
  var mapProp = {
    center: {lat:49.7222767, lng: 13.6322122},
    zoom:18,
    disableDefaultUI:true,
    mapTypeId:google.maps.MapTypeId.ROADMAP,
    styles: [{"featureType":"landscape","stylers":[{"saturation":-100},{"lightness":0},{"visibility":"on"}]},{"featureType":"poi","stylers":[{"saturation":-100},{"lightness":51},{"visibility":"simplified"}]},{"featureType":"road.highway",
		"stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"road.arterial","stylers":[{"saturation":-100},{"lightness":30},{"visibility":"on"}]},{"featureType":"road.local","stylers":[{"saturation":-100},{"lightness":40},
		{"visibility":"on"}]},{"featureType":"transit","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"administrative.province","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"labels","stylers":[{"visibility":"on"},
		{"lightness":-25},{"saturation":-100}]},{"featureType":"water","elementType":"geometry","stylers":[{"hue":"#ffff00"},{"lightness":-25},{"saturation":-97}]}]
    };

  var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);

  var marker=new google.maps.Marker({
    position:myCenter,
    icon:'graphics/images/pin_kontakt.png'
    });

  marker.setMap(map);
  }

  google.maps.event.addDomListener(window, 'load', initialize);


  </script>
	</head>
	<body>
	<a href='?page=home'><img src='graphics/images/logo_white_home.png' alt='Diginex logo' id='logo' /></a>

	<div class="nav">


		<ul>
		<a href='?page=o_nas'><li>Co je to DIGINEX</li></a>
		<a href='?page=tym'><li>Tým</li></a>
			<a href='?page=firma'><li>Řešení pro firmy</li></a>
			<a href='?page=vyvoj'><li>Vývoj</li></a>
				<a href='?page=internet'><li>Internet</li></a>
			<a href='?page=servis'><li>Servis</li></a>
			<a href='?page=kontakt'><li>Kontakt</li></a>
		</ul>
</div>
{% block content %}{% endblock %}
<footer> <div id='foot_efect'>&copy;2015 <a href='https://or.justice.cz/ias/ui/rejstrik-$firma?nazev=Diginex+Technology+s.r.o.' rel='nofollow' target='_blank' title='Obchodní rejstřík - externí stránka'>DIGINEX Technology s.r.o.</a> Všechna práva vyhrazena</div></footer>
<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

 </body>
</html>
