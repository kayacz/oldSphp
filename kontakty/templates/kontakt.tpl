{% extends "index.tpl" %}
{% block title %}Kontakt{% endblock %}
{% block content %}
<div id='content'>

		<div id='kontakt'>
    <div class='in_content1' id='in_first' >
      <div id="googleMap" style="position:relative;"></div>

		</div>

		<div id='content_middle'>
			<div class='pr'>
				<div class='pruh_kontakt pr_left_kontakt'></div>
				<div class='pruh pr_right'></div>
			</div>
			<div id='hex_kontakt'><p>
				Kontakt
			</p></div>
			<div class='pr'>
				<div class='pruh_dole_kontakt  pr_left_kontakt'></div>
				<div class='pruh_dole pr_right'></div>
			</div>
		</div>

		<div class='in_content' id='kontakt_text'>
	   <p >

       DIGINEX Technology s.r.o <br>
       Kocanda 20, <br>
       338 42 Kamenný Újezd <br>
       IČO: 040 12 291   <br>
        <br>
       Petr Pešek <br>
       +420 608 308 600 <br>
       petr.pesek@diginex.cz <br>
       <br>
       Luboš Hubáček<br>
       +420 608 308 600 <br>
       lubos.huback@diginex.cz <br>
       <br>

	   </p>
		</div>
	</div>
{% endblock %}
